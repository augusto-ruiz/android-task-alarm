package com.ruizcauich.taskalarm.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;

/**
 * This AlarmService class extends Service and implements OpPreparedListener
 * to play a sound in background.
 */
public class AlarmService
        extends Service
        implements MediaPlayer.OnPreparedListener {
    /**
     * Debugging tag
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * True if AlarmService is playing.
     */
    private static boolean mIsPlaying;

    /**
     * Plays a sound in the background.
     */
    private MediaPlayer mPlayer;

    /**
     * This factory method returns an explicit intent used to play and
     * stop playing the sound.
     */
    public static Intent makeIntent(Context context) {

        return new Intent(context, AlarmService.class);

    }

    /**
     * Hook method called when a new instance of Service is created.
     */
    @Override
    public void onCreate() {
        Log.i(TAG,"onCreate() entered");

        // Always call super class for necessary
        // initialization/implementation.
        super.onCreate();

        // Creats theMediaPlayer
        mPlayer = new MediaPlayer();

        // Indicate the MediaPlayer will stream the audio.
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    /**
     * Hook method called when the AlarmService is stopped.
     */
    @Override
    public void onDestroy() {

        // Stop playing.
        stop();

        // Call up to the super class.
        super.onDestroy();
    }

    /**
     * Hook method called every time startService() is called with an
     * Intent associated with this MusicService.
     */
    @Override
    public int onStartCommand(Intent intent,
                              int flags,
                              int startid) {
        // Web Service URL for the sound to play.
        final String url = "http://cd.textfiles.com/hackchronii/WAV/ALARM.WAV";


        if (mIsPlaying)
            // Stop playing the current song.
            stop();

        try {

            // Sets URL.
            mPlayer.setDataSource(url);

            // Register "this" listener.
            mPlayer.setOnPreparedListener(this);

            // This call doesn't block the UI Thread.
            mPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Don't restart Service if it shuts down.
        return START_NOT_STICKY;
    }

    /**
     * This no-op method is necessary.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Method called when Media Player is ready to play.
     */
    public void onPrepared(MediaPlayer player) {
        Log.i(TAG,"onPrepared() entered");

        // play repeatedly
        player.setLooping(true);

        // Now, it is playing
        mIsPlaying = true;

        // Start playing.
        player.start();
    }

    /**
     * Stops the MediaPlayer.
     */
    private void stop() {
        Log.i(TAG,"stop() entered");

        // Stop playing.
        mPlayer.stop();

        // Reset the state machine of the MediaPlayer.
        mPlayer.reset();

        // It's not playing anymore.
        mIsPlaying = false;
    }

}