package com.ruizcauich.taskalarm.logic;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * This contract class provides the needed
 * metadata for the Task Content Provider
 */
public final class TaskContract {

    /**
     * Defines the Authority identifier
     */
    public static final String CONTENT_AUTHORITY = "com.ruizcauich.taskalarm";

    /**
     * Creates the  task content provider  base URI
     */
    public static final Uri BASE_TASK_CONTENT_URI =
            Uri.parse("content://" + CONTENT_AUTHORITY);


    public static final class Tasks implements BaseColumns{
        /**
         * Name of the db table
         */
        public static final String TABLE_NAME   = "task_table";

        /**
         * This is the URI that apps will use to
         * access the Table via the Content Provider
         */
        public static final Uri CONTENT_URI =
            BASE_TASK_CONTENT_URI.buildUpon().appendPath(
                    Tasks.TABLE_NAME ).build();

        /**
         * Table column names used to store data
         */
        public static final String TITLE        = "title";
        public static final String MESSAGE      = "message";
        public static final String HOUR         = "hour";
        public static final String MINUTE       = "minute";

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/vnd."
                        + CONTENT_AUTHORITY
                        + "/"
                        + TABLE_NAME;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.dir.item/vnd."
                        + CONTENT_AUTHORITY
                        + "/"
                        + TABLE_NAME;

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id row id
         * @return Uri URI for the specified row id
         */
        public static Uri buildUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }



    }
}
