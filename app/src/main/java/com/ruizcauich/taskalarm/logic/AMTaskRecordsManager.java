package com.ruizcauich.taskalarm.logic;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.ruizcauich.taskalarm.receivers.AlarmReceiver;

/**
 * The AM (Alarm Manager) Task Records Manager
 * deals with the process of creating, deleting and updating
 * Task Alarms
 */
public final class AMTaskRecordsManager {

    private static final long REPETITION_INTERVAL = 1000 * 60 * 5; // 5 minutes

    /**
     * Creates a Alarm for the task which Uri (taskUri parameter)
     * is passed to the method
     * @param context
     * @param taskUri
     */
    public static void addTaskAlarm(Context context, Uri taskUri, long millis ){


        AlarmManager am = (AlarmManager) context.getSystemService( Context.ALARM_SERVICE );


        Intent intent = new Intent(context, AlarmReceiver.class);

        // Set the content Uri as data
        intent.setData( taskUri );

        // The following is for request code
        Task tempTask = Task.getTaskByUri(context, taskUri );
        // Request code is like an id for the pending intent
        int reqId = tempTask.getId(); // The task id will serve as request code


        // When save this id as extra for future use
        intent.putExtra("requestCodeId", reqId);

        // Creates a pending intent used to schedule the alarm
        PendingIntent sender = PendingIntent.getBroadcast(context,
                reqId, intent, 0);

        /**
         * Here the alarm is set up, as you can see, the Alarm Manager will
         * be triggering the broadcast every 5 minute
         */

        am.setRepeating(AlarmManager.RTC_WAKEUP,
                millis,
                REPETITION_INTERVAL, sender);

        Log.v( AMTaskRecordsManager.class.getName()
                , "Alarm was set up successfully");

    }

    /**
     * Remove or delete the Alarm which Uri (taskUri parameter)
     * is passed to the method
     * @param context
     * @param taskUri
     */
    public static void removeTaskAlarm(Context context, Uri taskUri){
        // Creates a implicit intent for AlarmReceiver

        Intent intent = new Intent(context, AlarmReceiver.class);

        // Set the content Uri as data
        intent.setData( taskUri );

        int requestCodeId = Task.getTaskByUri(
                context,taskUri)
                .getId();

        PendingIntent pi = PendingIntent.getBroadcast(context, requestCodeId, intent, PendingIntent.FLAG_CANCEL_CURRENT );
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        am.cancel(pi);

    }


}
