package com.ruizcauich.taskalarm.logic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ruizcauich.taskalarm.R;
import com.ruizcauich.taskalarm.utils.UiUtils;

public class TaskAdapter extends ArrayAdapter<Task> {

    private Context mContext;
    private Task[] mData;
    private int mResourceId;

    public TaskAdapter( Context context, int resource, Task[] objects) {
        super(context, resource, objects);

        // Save this references for future use
        this.mContext    =   context;
        this.mData       =   objects;
        this.mResourceId =   resource;

    }

    /**
     * getView implements the Holder pattern to have a
     * smoothy ListView scrolling, reusing rows views that ar not visible.
     * More info about this in
     * https://developer.android.com/training/improving-layouts/smooth-scrolling#java
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public  View getView(int position, View convertView, ViewGroup parent){
        // Previous row if exists. It can be converted to reuse
        View row = convertView;

        // Reference to a holder object that holds the xml components of a row
        Holder holder = null;

        // There weren't a previous row to reuse
        if( row == null ){
            // Expand the layout to use
            LayoutInflater inflater = LayoutInflater.from( mContext );
            row = inflater.inflate( mResourceId, parent, false);

            // Create a new holder to save the references
            holder = new Holder();

            // Get the components while setting them to the holder
            holder.taskTitle    =   (TextView) row.findViewById( R.id.viewTitle );
            holder.taskHour     =   (TextView) row.findViewById( R.id.viewHour );
            holder.taskMessage  =   (TextView) row.findViewById( R.id.viewMessage );

            //  Attach or link the holder object to its specific row
            row.setTag( holder );


        }else{
            holder  =   (Holder) row.getTag();
        }

        Task task = mData[position];
        holder.taskTitle.setText( task.getTitle() );
        holder.taskHour.setText( task.getHour() );
        holder.taskMessage.setText( task.getMessage() );


        return row;
    }


    private static class Holder{
        TextView taskTitle;
        TextView taskHour;
        TextView taskMessage;
    }
}
