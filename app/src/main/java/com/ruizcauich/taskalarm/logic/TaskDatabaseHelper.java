package com.ruizcauich.taskalarm.logic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;

/**
 * The database helper class (TaskDatabaseHelper) used by
 * the Alarm Content Provider to create
 * and manage its underlying SQLite database.
 */
public class TaskDatabaseHelper extends SQLiteOpenHelper {

    /**
     * Database name
     */
    public static final String DB_NAME = "ruiz_alarm_task_db.db";

    /**
     * Represents the current version of the database
     * Must be changed when a schema is modified
     */
    private static int DB_VERSION = 2;

    /**
     * SQL statement that defines the creation of
     * the Task table
     */
    final String SQL_CREATE_TASK_TABLE =
            "CREATE TABLE "
            + TaskContract.Tasks.TABLE_NAME + " ("
            + TaskContract.Tasks._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TaskContract.Tasks.TITLE + " TEXT NOT NULL, "
            + TaskContract.Tasks.MESSAGE + " TEXT NOT NULL, "
            + TaskContract.Tasks.HOUR + " INTEGER, "
            + TaskContract.Tasks.MINUTE + " INTEGER );";


    /**
     * Constructor that initialize the db name and version
     * @param context
     */
    public TaskDatabaseHelper(Context context){
        super(context,
                DB_NAME, null, DB_VERSION);


    }

    /**
     * Called when the db is created, creates the table.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Execute the create statement
        db.execSQL( SQL_CREATE_TASK_TABLE );
        Log.v(TaskDatabaseHelper.class.getName(), "Creating table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+
                TaskContract.Tasks.TABLE_NAME);

        onCreate(db);
    }
}
