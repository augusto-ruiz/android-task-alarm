package com.ruizcauich.taskalarm.logic;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class TaskProvider extends ContentProvider {

    /**
     * This tag is used to show log messages
     */
    private final static String TAG =
            TaskProvider.class.getName();

    /**
     * SQLite Open Helper to manage the db creation
     */
    private TaskDatabaseHelper mTaskDbHelper;

    /**
     * Context for content provider
     */
    private Context mContext;

    /**
     * Code return when a URI for more than 1 and
     * only 1 is returned
     */
    public static final int TASKS = 100;
    public static final int TASK = 101;

    private static final UriMatcher sUriMAatcher =
            buildUriMatcher();

    /**
     * Helper method to match each URI to the ACRONYM integers
     * constant defined above.
     *
     * @return UriMatcher
     */
    protected static UriMatcher buildUriMatcher() {
        // All paths added to the UriMatcher have a corresponding code
        // to return when a match is found.  The code passed into the
        // constructor represents the code to return for the rootURI.
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher =
                new UriMatcher(UriMatcher.NO_MATCH);

        // For each type of URI that is added, a corresponding code is
        // created.
        matcher.addURI(TaskContract.CONTENT_AUTHORITY,
                TaskContract.Tasks.TABLE_NAME,
                TASKS);
        matcher.addURI(TaskContract.CONTENT_AUTHORITY,
                TaskContract.Tasks.TABLE_NAME
                        + "/#",
                TASK);
        return matcher;
    }

    /**
     *
     * @return true id successfully started.
     */
    @Override
    public boolean onCreate() {
        mContext = getContext();

        mTaskDbHelper = new TaskDatabaseHelper( mContext );
        return true;
    }

    /**
     * This function gets data from the table
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     * @return
     */
    
    @Override
    public Cursor query( Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor cursor;
        SQLiteDatabase db = mTaskDbHelper.getWritableDatabase();

        cursor = db.query(TaskContract.Tasks.TABLE_NAME, projection, selection,
                selectionArgs,null, null,sortOrder);

        return cursor;

    }


    /**
     * Method called to handle type requests from client applications.
     * It returns the MIME type of the data associated with each
     * URI.
     */
    @Override
    public String getType( Uri uri) {
        switch (sUriMAatcher.match(uri)){
            case TASKS:
                return TaskContract.Tasks.CONTENT_ITEMS_TYPE;
            case TASK:
                return TaskContract.Tasks.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: "
                        + uri);
        }
    }

    /**
     * Insert data ino the table
     * @param uri
     * @param values
     * @return
     */
    @Override
    public Uri insert( Uri uri, ContentValues values) {
        Uri result;

        final SQLiteDatabase db = mTaskDbHelper.getWritableDatabase();

        long id = db.insert(TaskContract.Tasks.TABLE_NAME,
                null,
                values);
        if( id > 0){
            return TaskContract.Tasks.buildUri(id );

        }else{
            throw new android.database.SQLException
                    ("Failed to insert row into "
                            + uri);
        }

    }

    /**
     * Delete rows
     * @param uri
     * @param selection
     * @param selectionArgs
     * @return
     */
    @Override
    public int delete( Uri uri, String selection, String[] selectionArgs) {


        SQLiteDatabase db = mTaskDbHelper.getWritableDatabase();
        int nDeleted = db.delete(TaskContract.Tasks.TABLE_NAME, selection, selectionArgs);

        return nDeleted;
    }

    /**
     * Updates rows
     * @param uri
     * @param values
     * @param selection
     * @param selectionArgs
     * @return
     */
    @Override
    public int update( Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int nUpdated;
        SQLiteDatabase db = mTaskDbHelper.getWritableDatabase();

        nUpdated =
                db.update(TaskContract.Tasks.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        return nUpdated;
    }
}
