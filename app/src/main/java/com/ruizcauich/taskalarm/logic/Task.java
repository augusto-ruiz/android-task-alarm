package com.ruizcauich.taskalarm.logic;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.ruizcauich.taskalarm.utils.UiUtils;

/**
 * This class is used to define Alarm type object and
 * perform CRUD operations to maintain persistent data
 * into a SQLite Database wrapped by a Content Provider
 */
public class Task {
    /**
     * Attributes that represents the data of a Task
     */
    private  int mId;
    private String mTitle;
    private String mMessage;
    private int mHour;
    private int mMinute;

    /**
     * Used to get the ContentResolver
     */
    private Context mContext;

    /**
     * Reference to the most recent result of a query
     */
    private Cursor mCursor;

    /**
     * The ContentResolver used to get the Task Provider
     */
    private ContentResolver mContentResolver;

    /**
     *
     */
    private static final String[] COLUMNS_TO_DISPLAY = {
            TaskContract.Tasks._ID,
            TaskContract.Tasks.TITLE,
            TaskContract.Tasks.MESSAGE,
            TaskContract.Tasks.HOUR,
            TaskContract.Tasks.MINUTE,
    };

    /**
     * Constructor - Initialize needed data
     * for a Task and to perform CRUD operations
     * @param title
     * @param message
     */
    public Task(Context context, String title, String message,
                int hour, int minute){

        mContext = context;
        mTitle   = title;
        mMessage = message;
        mHour    = hour;
        mMinute  = minute;

        mContentResolver = context.getContentResolver();
    }

    private Task(Context context, int id, String title,
                 String message, int hour, int minute){
        this(context, title, message, hour, minute);

        mId = id;

    }

    /**
     * Deletes the current instance represented by an object of type Task
     * @return Number of rows deleted.
     */
    public int delete(){

        return mContentResolver.delete(TaskContract.Tasks.CONTENT_URI,
                TaskContract.Tasks._ID + " = ?",
                new String[]{ String.valueOf(mId)});
    }

    /**
     * Saves (INSERTS) the attributes of the object Task
     * into the SQLite database
     * @return
     */
    public Uri save(){
        final ContentValues cvs = new ContentValues();
        cvs.put(TaskContract.Tasks.TITLE, mTitle);
        cvs.put(TaskContract.Tasks.MESSAGE, mMessage);
        cvs.put(TaskContract.Tasks.HOUR, mHour);
        cvs.put(TaskContract.Tasks.MINUTE, mMinute);

        return mContentResolver.insert(TaskContract.Tasks.CONTENT_URI, cvs);

    }



    /**
     * Sets the non null params and executes a update in the db
     * @param title
     * @param message
     * @param hour
     * @param minute
     * @return number of rows updated
     */
    public int update(String title, String message, int hour, int minute){
        /**
         * Set the new values
         */
        if( title!=null )
            mTitle   = title;
        if( title!=null )
            mMessage = message;
        if( title!=null )
            mHour    = hour;
        if( title!=null )
            mMinute  = minute;
        /**
         * The current values
         */
        final ContentValues cvs = new ContentValues();
        cvs.put(TaskContract.Tasks.TITLE, mTitle);
        cvs.put(TaskContract.Tasks.MESSAGE, mMessage);
        cvs.put(TaskContract.Tasks.HOUR, mHour);
        cvs.put(TaskContract.Tasks.MINUTE, mMinute);

        String [] args = new String[]{ String.valueOf( mId ) };

        return mContentResolver.update(
                TaskContract.Tasks.CONTENT_URI,
                cvs,
                TaskContract.Tasks._ID + " = ?" , args);
    }

    /**
     * Helper method - Return the id of column name
     * @param c
     * @param colName
     * @return
     */
    private static int getColumId(Cursor c, String colName){
        return c.getColumnIndex(colName);
    }

    /**
     * Helper method that creates a Task from the data obtained
     * from a cursor, Context is used to create a Task
     * @param cursor
     * @param context
     * @return
     */
    private static Task getTaskFromCursor(Cursor cursor, Context context){
        int idColumn    = getColumId(cursor, TaskContract.Tasks._ID);
        int titleCol    = getColumId(cursor, TaskContract.Tasks.TITLE);
        int messCol     = getColumId(cursor, TaskContract.Tasks.MESSAGE);
        int hourCol     = getColumId(cursor, TaskContract.Tasks.HOUR);
        int minCol      = getColumId(cursor, TaskContract.Tasks.MINUTE);

        Task task = new Task(
                context,
                cursor.getInt(idColumn),
                cursor.getString(titleCol),
                cursor.getString(messCol),
                cursor.getInt(hourCol),
                cursor.getInt(minCol)
        );

        return task;
    }

    /**
     * This method is used to get a Task object
     * from a specific uri
     * @param uri
     * @return Corresponding task
     */
    public static Task getTaskByUri(Context context, Uri uri){
        // Extract the id portion
        int id = Integer.valueOf( uri.getLastPathSegment() );
        return getTaskById( context, id );

    }

    /**
     *
     * @param context
     * @param id
     * @return
     */
    public static Task getTaskById(Context context, int id){
        /**
         * Get the content resolver and execute a query
         */
        String[] args = { String.valueOf(id) };
        Cursor cursor =
                context.getContentResolver()
                .query(TaskContract.Tasks.CONTENT_URI, COLUMNS_TO_DISPLAY,
                        TaskContract.Tasks._ID + "= ?",args, null);

        /**
         * if zero, display a toast then exits from this method
         * while returning null to caller, otherwise, continue
         * to the next sentence
         */
        if( cursor.getCount() == 0){
            UiUtils.showShortToast(context, "No Task found");
            return null;
        }

        /**
         * Moves the row pointer to the first row
         */
        cursor.moveToFirst();

        /**
         * Gets a Task from the cursor and return it.
         */
        return getTaskFromCursor(cursor, context);
    }


    /**
     * Static method - Returns all stored tasks in a array
     * or null if no task is already stored.
     * @param context
     * @return
     * @throws RemoteException
     */
    public static Task[] getAllAlarms(Context context) {
        /**
         * Get the content resolver and execute a query with
         * selection, selectionArgs an sortOrder as nulls
         */
        Cursor cursor = context.getContentResolver()
                .query(TaskContract.Tasks.CONTENT_URI, COLUMNS_TO_DISPLAY,
                        null, null, null);

        // Get the count of entries obtained
        int count = cursor.getCount();
        /**
         * if zero, display a toast then exits from this method
         * while returning null to caller, otherwise, continue
         * to the next sentence
         */
        if( count == 0){
            UiUtils.showShortToast(context,"No tasks to display");
            return new Task[0];

        }

        /**
         * Creates a task array according with the number
         * of rows obtained before.
         */
        Task [] results = new Task[count];

        /**
         * used to place every task object in the array
         */
        int index = 0;


        /**
         * At the beginning, the row pointer is -1, therefore we must to
         * move to first or move to next row or a Exception will be thrown.
         * The while loop is used to go through the cursor
         */
        while( cursor.moveToNext() ){
            results[index]  = getTaskFromCursor(cursor, context);
            index++;
        }

        // Returns the array
        return results;
    }

    public int getId(){ return  mId;}

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getHour() {
        return String.format("%02d:%02d", mHour, mMinute);
    }

    public Uri getTaskUri(){ return TaskContract.Tasks.buildUri( this.mId );}

}
