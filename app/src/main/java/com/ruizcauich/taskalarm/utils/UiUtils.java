package com.ruizcauich.taskalarm.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

public class UiUtils {

    public static final void showShortToast(Context context, String message){
        Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
    }

    public static final void showLongToast(Context context, String message){
        Toast.makeText(context, message,Toast.LENGTH_LONG).show();
    }

    public static void showShortSnackbar(View v, String msg){
        Snackbar.make( v, msg, Snackbar.LENGTH_SHORT).show();
    }

    public static void showLongSnackbar(View v, String msg){
        Snackbar.make( v, msg, Snackbar.LENGTH_LONG).show();
    }

    public static boolean isEmpety(String s ){
        return (s.length() == 0);
    }

}
