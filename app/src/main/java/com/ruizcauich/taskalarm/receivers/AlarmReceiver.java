package com.ruizcauich.taskalarm.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.ruizcauich.taskalarm.activities.OngoingAlarmActivity;

/**
 * This will receive the broadcast triggered by the
 * Alarm Manager and then, the Ongoing Alarm activity
 * will be started.
 */
public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(this.getClass().getName(), "ALARMA!!!");
        /*
        MediaPlayer mp = MediaPlayer.create(context,
                Settings.System.DEFAULT_RINGTONE_URI);
        mp.start();*/

        /**
         * Get data and extras, where data is the uri of a
         * task in the content provider and extras has the
         * request code id
         */
        Uri data = intent.getData();
        Bundle extras = intent.getExtras();

        /*PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT );
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        am.cancel(pi);*/
        /**
         * Create a new intent to start OngoingAlarmActivity
         * where data and extras will be useful
         */
        Intent i = new Intent(context, OngoingAlarmActivity.class);
        i.setData( data );
        i.putExtras(extras);
        context.startActivity( i );

    }


}
