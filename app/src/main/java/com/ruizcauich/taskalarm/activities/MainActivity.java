package com.ruizcauich.taskalarm.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import com.ruizcauich.taskalarm.R;
import com.ruizcauich.taskalarm.logic.AMTaskRecordsManager;
import com.ruizcauich.taskalarm.logic.Task;
import com.ruizcauich.taskalarm.logic.TaskAdapter;
import com.ruizcauich.taskalarm.logic.TaskContract;
import com.ruizcauich.taskalarm.receivers.AlarmReceiver;
import com.ruizcauich.taskalarm.utils.UiUtils;

public class MainActivity extends LifecycleLoggingActivity {

    // To holds the list of tasks reference
    private ListView mList;

    // To hold the reference of the add task fab
    private FloatingActionButton mButtonAddTask;

    // Used to identify a result from add task activity
    private int ADD_TASK_REQUEST = 1;

    private Task storedTasks [];
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        storedTasks = new Task[] {
                new Task(this, "dfa", "adds", 1,1),
                new Task(this, "ddfa", "adasdfads", 1,1),
                new Task(this, "dfdafa", "aadsfdds", 1,1),
                new Task(this, "dfadsfa", "adddfs", 1,1),
        };

        // Set the references

        mList = (ListView) findViewById( R.id.listOfTasks);
        mButtonAddTask = (FloatingActionButton) findViewById(R.id.buttonAddTask);




        mButtonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(AddTaskActivity.makeIntent()
                , ADD_TASK_REQUEST);
            }
        });



        mContext = getApplicationContext();
        Log.v( TAG, "Context value is null : " + (mContext==null));
        if( mContext!= null){
            displayStoredTasks();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode == Activity.RESULT_OK){
            displayStoredTasks();
        }
    }



    /**
     * This method is called to display a Task List
     * Using Task objects array and a custom ArrayAdapter called
     * TaskAdapter
     */
    public void displayStoredTasks(){

        // Get stored tasks
        storedTasks = Task.getAllAlarms( mContext );

        TaskAdapter adapter = new TaskAdapter(getApplicationContext(),
                R.layout.row, storedTasks );

        mList.setAdapter( adapter );

        // Set a listener to execute when user taps a list item
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                // The task that the touched item represents
                final Task selectedTsk = storedTasks[position];

                // when user taps a item, the app will show a popup menu
                PopupMenu popup = new PopupMenu(MainActivity.this, view);

                // Inflate the proper layout for this popup menu
                popup.getMenuInflater().inflate(R.menu.options_menu, popup.getMenu());

                // To listen the users tap on a popup menu item
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Verify which item from the layout was selected
                        switch ( item.getItemId() ){
                            // The edit Task item
                            case R.id.edit_task:
                                Intent i = new Intent(MainActivity.this,
                                        AddTaskActivity.class);
                                i.setData( selectedTsk.getTaskUri() );

                                startActivityForResult(
                                        i,
                                        ADD_TASK_REQUEST
                                );

                                UiUtils.showShortToast(MainActivity.this,
                                        "Edit: " + selectedTsk.getTitle());
                                break;
                            // The delete task item
                            case R.id.delete_task:
                                AMTaskRecordsManager.removeTaskAlarm(
                                        MainActivity.this,
                                        selectedTsk.getTaskUri());

                                selectedTsk.delete();

                                UiUtils.showShortToast(MainActivity.this,
                                        "Deleted: " + selectedTsk.getTitle());

                                displayStoredTasks();
                        }
                        return true;
                    }
                });
                popup.show();

            }
        });

    }
}
