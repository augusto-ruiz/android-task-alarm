package com.ruizcauich.taskalarm.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ruizcauich.taskalarm.R;
import com.ruizcauich.taskalarm.logic.AMTaskRecordsManager;
import com.ruizcauich.taskalarm.logic.Task;
import com.ruizcauich.taskalarm.receivers.AlarmReceiver;
import com.ruizcauich.taskalarm.utils.UiUtils;

import java.util.Calendar;

public class AddTaskActivity extends LifecycleLoggingActivity {

    private EditText mEditTitle;
    private EditText mEditMessaage;
    private Button mSetAlarmHour;
    private TextView mViewHour;
    private Button mCancel;
    private Button mSave;



    private boolean timeWasSet = false;

    private int mHour;
    private int mMinute;

    // String that represents a Action name used to start this activity
    private static final String ADD_TASK = "com.ruizcauich.ADD_TASK";

    private static final int SELECT_HOUR_QUERY = 2;
    private static final int ALARM_MAX_REQUEST_CODE = 10000;

    // True when this activity is started for edit a Task
    private boolean mIsEditingMode = false;
    private Task mTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        /**
         * Get the reference of the View components
         */
        mEditTitle      = (EditText)  findViewById( R.id.editTitle);
        mEditMessaage   = (EditText)  findViewById( R.id.editMessage);
        mSetAlarmHour   = (Button)    findViewById(R.id.buttonSetAlarmHour);
        mViewHour       = (TextView)  findViewById(R.id.viewHour);
        mCancel         = (Button)    findViewById(R.id.buttonCancel);
        mSave           = (Button)    findViewById(R.id.buttonSave);

        /**
         * Set three listeners for every
         * button we have in the layout
         */

        /**
         * Set alarm hour
         */
        mSetAlarmHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSelectHourActivity();
            }
        });

        /**
         * Cancel button
         */
        mCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        /**
         * Save button
         */
        mSave.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                save();
            }
        });

        /**
         * If this activity was started to edit a existing task
        * */
        Intent intent = getIntent();
        // Ther is a Uri to the task
        Uri taskUri = intent.getData();
        if( taskUri!= null){
            // set the mode to editing
            mIsEditingMode  = true;
            mTask = Task.getTaskByUri(this, taskUri );
            initViewValues();
        }

    }

    /**
     * Method that initialize al the views from the mTask
     * data
     */
    private void initViewValues(){
        // Set title and message
        mEditTitle.setText( mTask.getTitle() );
        mEditMessaage.setText( mTask.getMessage() );

        // Splits the hour String (HH:MM) returned by getHour
        // To set hour and minute
        mHour = Integer.valueOf(
                mTask.getHour()
                .split(":")[0]
        );
        mMinute = Integer.valueOf(
                mTask.getHour()
                        .split(":")[1]
        );

        mViewHour.setText( mTask.getHour() );


    }


    /**
     * Factory method used to create a implicit intent
     * that points to this activity
     * @return the intent
     */
    public static Intent makeIntent(){
        Intent i = new Intent( ADD_TASK) ;

        return i;
    }



    /**
     * This method starts the Select hour activity
     */
    private void startSelectHourActivity(){
        Intent i = new Intent(this, SelectHourActivity.class);

        startActivityForResult(i, SELECT_HOUR_QUERY);

    }

    /**
     * Finish this activity while setting CANCELED
     * as result code
     */
    private void cancel(){
        setResult( Activity.RESULT_CANCELED );
        finish();

    }

    /**
     * This function will setup a alarm
     * @param data
     */
    private void setupAlarm(Uri data){
        //We need a calendar object to get the specified time in millis
        //as the alarm manager method takes time in millis to setup the alarm
        Calendar calendar = Calendar.getInstance();

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), mHour, mMinute, 0);

        // If is editing a existing task, first remove the
        // previous registered alarm
        if( mIsEditingMode ){
            AMTaskRecordsManager.removeTaskAlarm(this, data);
        }

        AMTaskRecordsManager.addTaskAlarm(this, data, calendar.getTimeInMillis());

    }


    /**
     * Executed when user press save
     */
    private void save(){
        // Get the data
        String title    = mEditTitle.getText().toString();
        String msg      = mEditMessaage.getText().toString();

        /**
         * Verify if user has set the time to trigger the alarm
         */
        if( !timeWasSet ){
            UiUtils.showShortSnackbar(mSave, "Please, set alarm hour!");
            return;
        }
        /**
         * If fields are empty, we can not proceed
         */
        if( UiUtils.isEmpety(title) ||
                UiUtils.isEmpety(msg) ){
            UiUtils.showShortSnackbar(mSave, "Please, set title and message!");
            return;
        }

        // Needed to create the alarm
        Uri uri;
        /**
         * Updates or Creates and store a task and then call setupAlarm to
         * set up the alarm using alarm manager.
         */
        if( mIsEditingMode ){
            // Is editing a existing task
            mTask.update( title, msg, mHour, mMinute );
            uri = mTask.getTaskUri();
        }
        else{

            // Creates a new Task Object
            mTask = new Task(this, title, msg, mHour, mMinute);
            // Save this message to DB
            uri = mTask.save();
        }


        /**
         * Call the setup alarm helper function
         */
        setupAlarm( uri );
        Log.v(TAG, "Saved: " + uri.toString());

        /**
         * Set code result as Ok and finish this activity
         */
        setResult(Activity.RESULT_OK);
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /**
         * if no select hour canceled, show message and
         * do nothing
         */
        if( resultCode== Activity.RESULT_CANCELED ){
            showWarningMessage( "Please, set time to proceed and create the Task Alarm." );
            return;
        }

        /**
         * Otherwise, continue
         */
        // the user has set the time
        timeWasSet = true;

        /**
         * Gets the data from extras
         */
        Bundle b = data.getExtras();

        mHour   = b.getInt("hour");
        mMinute = b.getInt("minute");

        /**
         * Creates a formatted String object like 20:05
         */
        String f = String.format( "%02d:%02d", mHour, mMinute);
        mViewHour.setText( f );

    }

    private void showWarningMessage( String msg ){
        UiUtils.showLongSnackbar( mSave, msg);
    }
}
