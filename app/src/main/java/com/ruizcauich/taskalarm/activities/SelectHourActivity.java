package com.ruizcauich.taskalarm.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import com.ruizcauich.taskalarm.R;

public class SelectHourActivity extends LifecycleLoggingActivity {

    private TimePicker mTimePicker;
    private Button mCancel;
    private Button mSaveHour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_hour);

        mTimePicker     = (TimePicker) findViewById(R.id.timePicker);
        mCancel         = (Button) findViewById(R.id.buttonCancel);
        mSaveHour       = (Button) findViewById(R.id.buttonSaveHour);

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        mSaveHour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveHour();
            }
        });
    }

    private void cancel(){

        setResult( Activity.RESULT_CANCELED );
        finish();
    }

    private void saveHour(){
        int hour;
        int minute;

        /**
         * From Android version 23 and latter, how to
         * get time data from TimePicker has changed
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hour    = mTimePicker.getHour();
            minute  = mTimePicker.getMinute();
        }else{
            hour    = mTimePicker.getCurrentHour();
            minute  = mTimePicker.getCurrentMinute();
        }

        /**
         * Creates a new intent  and attach
         * hour and minute to it
         */
        Intent res  = new Intent();
        res.putExtra("hour", hour);
        res.putExtra("minute", minute);

        // Sets the result
        setResult(Activity.RESULT_OK, res);

        // Exits from this Activity
        finish();
    }
}
