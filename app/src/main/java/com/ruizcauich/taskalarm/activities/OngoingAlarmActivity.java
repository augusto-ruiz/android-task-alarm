package com.ruizcauich.taskalarm.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ruizcauich.taskalarm.R;
import com.ruizcauich.taskalarm.logic.AMTaskRecordsManager;
import com.ruizcauich.taskalarm.logic.Task;
import com.ruizcauich.taskalarm.receivers.AlarmReceiver;
import com.ruizcauich.taskalarm.service.AlarmService;

public class OngoingAlarmActivity extends LifecycleLoggingActivity{


    private TextView  viewMessage;

    private Uri taskUri;

    private int requestCodeId;

    private Intent mAlarmServiceIntent;

    Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing_alarm);

        taskUri = getIntent().getData();

        viewMessage = findViewById(R.id.viewMessage);

        try {
            task = Task.getTaskByUri(this, taskUri);
            viewMessage.setText( task.getMessage() );
        }catch (Exception e) {

        }

        requestCodeId = getIntent().getExtras().getInt("requestCodeId");
        playAlarmService();


    }

    public void stopAlarm(View v){
        Log.v(this.TAG, "Stopping alarm");
        stopAlarmService();

        finish();

    }

    public void stopAndDeleteAlarm(View v){
        Log.v(this.TAG, "Deleting alarm");

        // Call to remove the remove the alarm
        AMTaskRecordsManager.removeTaskAlarm(this, taskUri);

        try {
            // Delete the task from de db
            task.delete();
        }catch (Exception e){}

        // Stop playing the sound
        stopAlarmService();

        // Exits this activity
        finish();


    }

    public void playAlarmService(){
        this.mAlarmServiceIntent = AlarmService.makeIntent(this);
        startService(this.mAlarmServiceIntent);

    }
    public void stopAlarmService(){
        if( mAlarmServiceIntent!=null ){
            stopService( mAlarmServiceIntent );
        }
    }


}
