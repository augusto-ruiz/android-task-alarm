# Task Alarm App for Android

This **project** for the **_Capstone MOOC_** (MOOC number 5) of the Android App Development specialization has the following objective:

- Keep control of pending tasks and handle alarms that are triggered at the time specified by the user.

To accomplish the mentioned objective, there are several android components created for each feature this app provide. Especially the ones related to the user interface, therefore, you can find in this project four activities each one with its respective layout defiened in XML files.

## Features
Here is a description of the feature related with every designed Activity:

<dl>
  <dt>Task list - MainActivity</dt>
  <dd>The first screen the user interact with is a list that shows all pending tasks which has been registered, if no task is in the database, the user only will see a Floating Action Button to add one pendding task.</dd>

  <dt>Add new task - AddTaskActivity</dt>
  <dd>Clicking the add button on the first screen, the app will swap to the <strong>Add Task Activity</strong>, where the user will be prompted to provide a <strong>title</strong> (name of task) and a <strong>description</strong> to show when the alarm is triggered.<br>
Provide a specific hour to configure the alarm is needed too through a third screen.</dd>

<dt>Set alarm hour - SelectHourActivity</dt>
  <dd>To set the time when the alarm will be triggered, a third screen is shown. In this screen, the user can select both, hour and minute using a time picker view.</dd>

<dt>Stop alarm - StopAlarmActivity</dt>
<dd>When the <strong>OngoingAlarmReceiver</strong> handdle a broadcast intent, the StopAlarmAvtivity is started. This activity, starts a service to play a sound, and from this activity, user will be prompted to stop and delete the corresponding pending task or just stop the alarm. </dd>
</dl>

## About the Web service
When a alarm broadcast is fired, the StopAlarmActivity is called and this activity starts a Service, this service connect with a web service that provides a sound to play in background.

This is the url used by the service: http://cd.textfiles.com/hackchronii/WAV/ALARM.WAV


